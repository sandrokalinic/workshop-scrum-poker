import io from 'socket.io-client';
import settings from '../settings.json';

const socket = io(settings.serverPort);

socket.on('connect', () => {
	console.log('Connected to the server');
});

export default socket;
