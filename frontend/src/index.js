import React from 'react';
import ReactDOM from 'react-dom';
import AppRouter from './AppRouter';
import 'semantic-ui-css/semantic.min.css';

ReactDOM.render(<AppRouter />, document.getElementById('root'));
