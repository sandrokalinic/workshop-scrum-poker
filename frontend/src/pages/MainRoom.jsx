import React, { Component } from 'react';
import { Segment, Grid, Container, Button, List, SegmentGroup, Item, ItemHeader } from 'semantic-ui-react';
import socket from '../utils/socket';
import Cards from '../components/MainRoom/PlayerCards';
import Voter from '../components/general/Voter';
import AddStory from '../components/MainRoom/AddStory';
import QrModal from '../components/MainRoom/QrModal';

export default class extends Component {
	roomId = this.props.match.params.roomId;

	// State
	state = {
		roomName: '',
		voters: [],
		topic: null,
		showVotes: false
	};

	// State updaters
	setRoomName = (roomName) => {
		this.setState({ ...this.state, roomName });
	};
	setVoters = (voters) => {
		this.setState({ ...this.state, voters });
	};
	setTopic = (topic) => {
		this.setState({ ...this.state, topic });
	};
	toggleVotes = (showVotes) => {
		this.setState({ ...this.state, showVotes });
	};

	// LIfecycle methods
	componentDidMount() {
		const { roomId, setRoomName, setTopic, setVoters } = this;
		const userId = localStorage.getItem('userId');

		// Init the voting room
		socket.emit('master_rejoin', { roomId, userId }, (data) => {
			const { roomName = '', voters = [], currentTopic = '' } = data;
			setRoomName(roomName);
			setTopic(currentTopic);
			setVoters(voters);
		});

		// Setup socket event handlers
		socket.on('topic_changed', this.topicChanged);
		socket.on('voter_joined', this.voterJoined);
		socket.on('voter_action', this.voterAction);
		socket.on('vote_finished', this.voteFinished);
	}
	componentWillUnmount() {
		// Remove socket event handlers
		socket.removeListener('topic_changed', this.topicChanged);
		socket.removeListener('voter_joined', this.voterJoined);
		socket.removeListener('voter_action', this.voterAction);
		socket.removeListener('vote_finished', this.voteFinished);
	}

	// Methods
	// Topic
	topicChanged = ({ topic }) => {
		this.setTopic(topic);
		this.toggleVotes(false);
	};
	restartTopic = () => {
		this.setTopic(this.state.topic);
		this.toggleVotes(false);
	};

	// Voters
	voterJoined = (voter) => {
		const newVoters = [ ...this.state.voters, voter ];
		this.setVoters(newVoters);
		this.restartTopic();
	};
	voterAction = (data) => {
		const { voterId, type, value } = data;
		const { voters } = this.state;

		if (type === 'vote') {
			const updatedVoters = voters.map((voter) => {
				if (voter.voterId === voterId) {
					return { ...voter, vote: value };
				} else return voter;
			});

			this.setVoters(updatedVoters);
		} else if (type === 'left') {
			const updatedVoters = voters.filter((voter) => voter.voterId !== voterId);
			this.setVoters(updatedVoters);
		}
	};
	voteFinished = ({ voters }) => {
		this.setVoters(voters);
		this.toggleVotes(true);
	};

	render() {
		const { roomId, state, restartTopic } = this;
		const { voters, roomName, topic, showVotes } = state;

		return (
			<Container fluid className="mainRoom_container">
				<Segment placeholder className="main_segment">
					<Grid columns={2} stackable>
						<Grid.Row className="grid_row">
							<Grid.Column width={11}>
								<AddStory />
								<div className="cards-container">
									{voters &&
										voters.map(({ voterId, voterName, vote }, i) => (
											<Cards
												username={voterName}
												id={i + 1}
												vote={vote}
												showVotes={showVotes}
												key={voterId}
											/>
										))}
								</div>
							</Grid.Column>
							<Grid.Column width={4}>
								<Segment.Group>
									<Segment textAlign="left" className="mainRoom_side-header">
										<Container fluid>{roomName}:</Container>
										<br />
										<Container fluid>{topic ? topic : 'Add story to start voting'}</Container>
									</Segment>
									<Segment>
										<Button.Group fluid attached="bottom">
											{topic && (
												<Button onClick={restartTopic} size="huge" color="teal">
													Restart
												</Button>
											)}
										</Button.Group>
									</Segment>
									<Segment>
										<Container fluid className="timer_container">
											<div>voters:</div>
										</Container>
									</Segment>
									<Segment>
										<List animated size="large" verticalAlign="middle">
											{voters &&
												voters.map(({ voterId, voterName, voterAvatar }) => (
													<Voter
														username={voterName}
														voterAvatar={voterAvatar}
														key={voterId}
													/>
												))}
										</List>
									</Segment>
								</Segment.Group>
								<SegmentGroup>
									<Segment className="code_segment">
										<Container fluid textAlign="center">
											Invite members
										</Container>
									</Segment>
									<Segment>
										<Item.Group unstackable>
											<Item>
												<QrModal />
												<div className="spacer" />
												<Item.Content className="qr_text">
													<Item.Header as="h4">Use sesion ID code:</Item.Header>
													<ItemHeader className="id_header" as="h5">
														{roomId}
													</ItemHeader>
													<Item.Description>
														<p>Or scan this QR code with your phone</p>
													</Item.Description>
												</Item.Content>
											</Item>
										</Item.Group>
									</Segment>
								</SegmentGroup>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Segment>
			</Container>
		);
	}
}
