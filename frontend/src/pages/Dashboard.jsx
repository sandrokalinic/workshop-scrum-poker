import React from 'react';
import { Container, Header } from 'semantic-ui-react';
import CreateOrJoin from '../components/Dashboard/CreateOrJoin';
import Join from '../components/Dashboard/Join';
import Acordion from '../components/Dashboard/Acordion';

const Dashboard = ({ match }) => {
	const { roomId } = match.params;

	return (
		<Container>
			<Container className="headers_container">
				<Header as="h1">
					Pure & Simple Planning
					<Header.Subheader className="sub_header">
						Make Estimating Agile Projects Accurate & Fun
					</Header.Subheader>
				</Header>
			</Container>
			{!roomId ? <CreateOrJoin /> : <Join id={roomId} />}
			<Container className="acordion_container">
				<Header as="h2">Alea Scrum Poker Guide</Header>
				<Acordion />
			</Container>
		</Container>
	);
};

export default Dashboard;
