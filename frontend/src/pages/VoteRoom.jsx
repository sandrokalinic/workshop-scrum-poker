import React, { useState, useEffect } from 'react';
import socket from '../utils/socket';
import { history } from '../AppRouter';
import ScrumCards from '../components/VoteRoom/ScrumCards';
import Results from '../components/VoteRoom/Results';
import fibonacci from '../utils/fibonacci';

const VoteRoom = ({ match }) => {
	const { roomId, voterName } = match.params;
	const [ topic, setTopic ] = useState(null);
	const [ vote, setVote ] = useState(null);
	const [ voters, setVoters ] = useState([]);
	const [ showVotes, toggleVotes ] = useState(false);

	// Topic
	const topicChanged = ({ topic }) => {
		setTopic(topic);
		toggleVotes(false);
		setVote(null);
	};

	// Voting
	const handleVote = (value) => {
		if (vote || !topic) return;
		socket.emit('vote', { vote: value }, ({ success }) => {
			success && setVote(value);
		});
	};
	const voteFinished = ({ voters }) => {
		setVoters(voters);
		toggleVotes(true);
	};

	useEffect(() => {
		// Join the room when the page mounts
		socket.emit('join_room', { roomId, voterName }, ({ error, topic }) => {
			if (error) {
				alert('Error occurred!');
				history.push('/');
			} else setTopic(topic);
		});

		// Setup socket event handlers
		socket.on('topic_changed', topicChanged);
		socket.on('vote_finished', voteFinished);

		// Remove socket event handlers
		return () => {
			socket.removeListener('topic_changed', topicChanged);
			socket.removeListener('vote_finished', voteFinished);
		};
	}, []);

	return (
		<React.Fragment>
			{showVotes ? (
				<Results voters={voters} />
			) : (
				<div>
					<div className="vote_story">
						<h1>{topic ? topic : 'Waiting for topic'}</h1>
					</div>
					<div className="cards-container scrum_container">
						{fibonacci.map((value) => (
							<ScrumCards handleVote={handleVote} vote={vote} value={value} key={value} />
						))}
					</div>
				</div>
			)}
		</React.Fragment>
	);
};

export default VoteRoom;
