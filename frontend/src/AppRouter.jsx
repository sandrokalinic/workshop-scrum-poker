import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import AppMenu from './components/general/AppMenu';
import Dashboard from './pages/Dashboard';
import MainRoom from './pages/MainRoom';
import VoteRoom from './pages/VoteRoom';

export const history = createBrowserHistory();

const AppRouter = () => (
	<Router history={history}>
		<div id="app">
			<AppMenu />
			<Switch>
				<Route path="/" component={Dashboard} exact={true} />
				<Route path="/room/:roomId" component={MainRoom} />
				<Route path="/vote/:roomId/:voterName" component={VoteRoom} />
				<Route path="/join/:roomId" component={Dashboard} />
				<Route path="*" component={Dashboard} />
			</Switch>
		</div>
	</Router>
);

export default AppRouter;
