import React from 'react';
import { Container, Segment, Grid, List } from 'semantic-ui-react';
import { Doughnut } from 'react-chartjs-2';
import Voter from '../general/Voter';

const Results = ({ voters = [] }) => {
	const data = {
		labels: voters.map((player) => player.voterName),
		datasets: [
			{
				data: voters.map((player) => player.vote),
				backgroundColor: [
					'#FF6384',
					'#b388ff',
					'#FFCE32',
					'#F58B27',
					'#f44336',
					'#00bcd4',
					'#76ff03',
					'#546e7a',
					'#ff9e80',
					'#EDF2F7'
				],
				hoverBackgroundColor: [
					'#FF6384',
					'#b388ff',
					'#FFCE32',
					'#F58B27',
					'#f44336',
					'#00bcd4',
					'#76ff03',
					'#546e7a',
					'#ff9e80',
					'#EDF2F7'
				]
			}
		]
	};

	return (
		<Container>
			<Grid relaxed>
				<Grid.Row className="result_grid">
					<Grid.Column width={4} />
					<Grid.Column width={8}>
						<div>
							<Doughnut
								data={data}
								width={450}
								height={300}
								options={{
									title: {
										display: true,
										text: 'Vote Results',
										fontSize: 16
									},
									legend: {
										display: true,
										position: 'top',
										labels: {
											boxWidth: 25,
											fontSize: 15,
											padding: 5
										}
									},
									animation: {
										duration: 1500
									},
									maintainAspectRatio: false
								}}
							/>
						</div>
						<Segment>
							<List animated size="medium" verticalAlign="middle">
								{voters.map(({ voterId, voterName, vote, voterAvatar }) => (
									<Voter vote={vote} username={voterName} voterAvatar={voterAvatar} key={voterId} />
								))}
							</List>
						</Segment>
					</Grid.Column>
					<Grid.Column width={4} />
				</Grid.Row>
			</Grid>
		</Container>
	);
};

export default Results;
