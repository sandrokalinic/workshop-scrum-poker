import React from 'react';

const ScrumCards = ({ handleVote, vote, value }) => {
	const selected = vote === value ? ' selected' : '';

	return (
		<React.Fragment>
			<div className="card_container" onClick={() => handleVote(value)}>
				<div className={'card scrum_card' + selected}>
					<div className="card_header">{value}</div>
					<div className="card_content">{value}</div>
					<div className="card_footer">{value}</div>
				</div>
			</div>
		</React.Fragment>
	);
};

export default ScrumCards;
