import React, { Component, createRef } from 'react';

const { QRCode } = window;

export default class extends Component {
	qrcode = createRef();

	componentDidMount() {
		const { width = 120, height = 120 } = this.props;
		const text = window.location.href.replace('room', 'join');
		const options = {
			text,
			width,
			height,
			colorDark: '#00aaff',
			colorLight: '#ffffff'
		};
		console.log('_____', text);
		setTimeout(() => {
			new QRCode(this.qrcode.current, options);
		}, 0);
	}

	render() {
		return <div ref={this.qrcode} />;
	}
}
