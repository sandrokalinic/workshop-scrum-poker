import React from 'react';
import { Modal, Item } from 'semantic-ui-react';
import QRCode from './QRCode';

const QrModal = () => {
	return (
		<Modal
			trigger={
				<Item>
					<QRCode />
				</Item>
			}
			centered={false}
			size="tiny"
		>
			<Modal.Header>Scan QR Code to enter vote room</Modal.Header>
			<Modal.Content image>
				<Modal.Description>
					<QRCode width={400} height={400} />
				</Modal.Description>
			</Modal.Content>
		</Modal>
	);
};

export default QrModal;
