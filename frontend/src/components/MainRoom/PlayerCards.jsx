import React from 'react';

const Cards = ({ username, id, vote, showVotes }) => {
	const showClass = showVotes ? 'rotate_container' : '';
	const votedClass = vote ? 'voted' : '';
	return (
		<React.Fragment>
			<div className={`card_container ${showClass}`}>
				<div className={`card card_front ${votedClass}`}>
					<div className="card_header">{id}</div>
					<div className="card_content card_content-small">
						<p>{username}</p>
					</div>
					<div className="card_footer">{id}</div>
				</div>
				<div className="card card_back">
					<div className="card_header">{id}</div>
					<div className="card_content card_content-small">
						<p>{username}</p>
						<p>{vote}</p>
					</div>
					<div className="card_footer">{id}</div>
				</div>
			</div>
		</React.Fragment>
	);
};

export default Cards;
