import React, { useState } from 'react';
import { Form } from 'semantic-ui-react';
import socket from '../../utils/socket';

const AddStory = () => {
	const [ topic, setTopic ] = useState('');

	const handleChange = ({ target }) => setTopic(target.value);

	const createTopic = () => {
		socket.emit('next_topic', { topic }, ({ sucess }) => {
			sucess && setTopic('');
		});
	};

	return (
		<Form>
			<Form.Group>
				<Form.Input
					className="story_input"
					placeholder="Create new story"
					name="topic"
					value={topic}
					onChange={handleChange}
				/>
				<Form.Button className="story_btn" color="blue" onClick={createTopic} content="Create" />
			</Form.Group>
		</Form>
	);
};

export default AddStory;
