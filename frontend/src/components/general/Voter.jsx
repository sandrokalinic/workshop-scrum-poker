import React from 'react';
import { List, Image } from 'semantic-ui-react';

const Voter = ({ username, voterAvatar, vote }) => {
	return (
		<React.Fragment>
			<List.Item>
				<Image avatar src={voterAvatar} />
				<List.Content>
					<List.Header>
						{username} {vote ? ` - ${vote}` : ''}
					</List.Header>
				</List.Content>
			</List.Item>
		</React.Fragment>
	);
};

export default Voter;
