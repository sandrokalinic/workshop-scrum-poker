import React from 'react';
import { Header, Menu, Container } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

const AppMenu = () => {
	return (
		<div>
			<Menu fixed="top" inverted>
				<Container fluid>
					<Menu.Item as={Link} to="/" header className="menu_left-item">
						<Header as="h2" image="/img/alea-logo-white.png" content="Scrum Poker" color="blue" />
					</Menu.Item>
				</Container>
			</Menu>
		</div>
	);
};

export default AppMenu;
