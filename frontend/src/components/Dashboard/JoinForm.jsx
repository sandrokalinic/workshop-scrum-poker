import React, { useState } from 'react';
import { Button, Form, Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';

export default ({ id = '' }) => {
	const [ roomId, setRoomId ] = useState(id);
	const [ username, setUsername ] = useState('');

	// Validation
	const validRoomId = roomId && roomId.length > 4 ? false : true;
	const validUsername = username && username.length > 2 ? false : true;

	// Change the room id
	const handleRoomChange = ({ target }) => {
		setRoomId(target.value);
	};

	// Change the username
	const handleUsernameChange = ({ target }) => {
		setUsername(target.value);
	};

	return (
		<Grid.Column className="join_column">
			<Form size="big">
				<Form.Input
					icon="barcode"
					iconPosition="left"
					label="Sesion ID:"
					placeholder="12345"
					value={roomId}
					onChange={handleRoomChange}
				/>
				<Form.Input
					icon="user"
					iconPosition="left"
					label="Your Name"
					placeholder="John"
					value={username}
					onChange={handleUsernameChange}
				/>
				<Button
					disabled={validRoomId || validUsername}
					as={Link}
					to={`/vote/${roomId}/${username}`}
					content="Join"
					icon="signup"
					color="blue"
					size="medium"
				/>
			</Form>
		</Grid.Column>
	);
};
