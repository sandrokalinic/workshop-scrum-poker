import React, { Component } from 'react'
import { Accordion, Icon } from 'semantic-ui-react'

export default class AccordionExampleStandard extends Component {
  state = { activeIndex: 0 }

  handleClick = (e, titleProps) => {
    const { index } = titleProps
    const { activeIndex } = this.state
    const newIndex = activeIndex === index ? -1 : index

    this.setState({ activeIndex: newIndex })
  }

  render() {
    const { activeIndex } = this.state

    return (
      <Accordion>
        <Accordion.Title active={activeIndex === 0} index={0} onClick={this.handleClick}>
          <Icon name='dropdown' />
          What is a Scrum poker?
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 0}>
          <p>
            Scrum poker or planning poker is a consensus based, gamified technique to estimate the complexity and effort of a software feature
          </p>
        </Accordion.Content>

        <Accordion.Title active={activeIndex === 1} index={1} onClick={this.handleClick}>
          <Icon name='dropdown' />
          How it works?
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 1}>
          <p>
            After presenting the feature, each member of the team picks a vote from different ranges of card sets. The vote remains hidden until all members have voted to avoid influence from other team members. After everyone has voted the highest and lowest estimates explain their choice and the process is repeated until the team agrees on a value.
          </p>
        </Accordion.Content>

        <Accordion.Title active={activeIndex === 2} index={2} onClick={this.handleClick}>
          <Icon name='dropdown' />
          How to start a new session?
        </Accordion.Title>
        <Accordion.Content active={activeIndex === 2}>
          <p>
            U can create new session and share ID to the team members or u can join existing session with the provided ID
          </p>
        </Accordion.Content>
      </Accordion>
    )
  }
}