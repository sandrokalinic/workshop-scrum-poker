import React, { useState } from 'react';
import { Button, Form, Grid } from 'semantic-ui-react';
import socket from '../../utils/socket';
import { history } from '../../AppRouter';

export default () => {
	const [ roomName, setRoomName ] = useState('');
	const validRoomId = roomName && roomName.length > 2 ? false : true;

	// Change the room name
	const handleChange = ({ target }) => {
		setRoomName(target.value);
	};

	// Send room create request to the server
	const createRoom = () => {
		socket.emit('create_room', { roomName }, ({ error, roomId, userId }) => {
			if (error) alert('Error occurred!');
			else {
				localStorage.setItem('userId', userId);
				history.push('/room/' + roomId);
			}
		});
	};

	return (
		<Grid.Column className="create_column">
			<Form size="big">
				<Form.Input
					icon="home"
					iconPosition="left"
					label="Room name:"
					placeholder="12345"
					value={roomName}
					onChange={handleChange}
				/>
				<Button disabled={validRoomId} onClick={createRoom} content="Create" primary size="medium" icon="add" />
			</Form>
		</Grid.Column>
	);
};
