import React from 'react';
import { Grid, Segment, Container } from 'semantic-ui-react';
import Join from './JoinForm';

const CreateOrJoin = ({ id }) => {
	return (
		<Container>
			<Segment className="hero_segment" placeholder>
				<Grid columns={1} relaxed="very" stackable>
					{/* Join from */}
					<Join id={id} />
				</Grid>
			</Segment>
		</Container>
	);
};

export default CreateOrJoin;
