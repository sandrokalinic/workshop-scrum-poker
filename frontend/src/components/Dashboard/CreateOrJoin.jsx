import React from 'react';
import { Divider, Grid, Segment, Container } from 'semantic-ui-react';
import Create from './CreateForm';
import Join from './JoinForm';

const CreateOrJoin = () => {
	return (
		<Container>
			<Segment className="hero_segment" placeholder>
				<Grid columns={2} relaxed="very" stackable>
					{/* Create form */}
					<Create />
					{/* Join from */}
					<Join />
				</Grid>
				<Divider className="hide_divider" vertical>
					Create Or Join
				</Divider>
			</Segment>
		</Container>
	);
};

export default CreateOrJoin;
