import sys
import logging
import tornado
from core.socketioserver import sio_handler


def create_app(handler):
    app = tornado.web.Application(
        [
            (r"/socket.io/", handler),
        ],
    )
    return app


if __name__ == '__main__':
    logging.getLogger("tornado.access").setLevel(logging.WARN)
    logging.basicConfig(level=logging.DEBUG,
                        stream=sys.stdout)
    app = create_app(sio_handler)
    app.listen(8080)
    tornado.ioloop.IOLoop.current().start()
