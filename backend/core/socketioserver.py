import socketio
from core.scrumpoker import ScrumPoker
from core.util import rand_id, rand_img, PokerStore
import logging

log = logging.getLogger('ws')
sio = socketio.AsyncServer(async_mode='tornado',
                           logger=log)
sio_handler = socketio.get_tornado_handler(sio)


@sio.on('connect')
async def connect(sid, environ):
    async with sio.session(sid) as session:
        if 'user_id' not in session:
            session['user_id'] = rand_id()
            session['avatar'] = rand_img()
        user_id = session['user_id']
    log.debug(f'Client connected: {sid} (id: {user_id})')


@sio.on('disconnect')
async def disconnect(sid):
    session = await sio.get_session(sid)
    user_id = session['user_id']
    for room in sio.rooms(sid):
        if room != sid:
            try:
                p = PokerStore.get_poker(room)
                p.remove_member(user_id)
                await sio.emit('voter_action',
                               dict(type='left',
                                    value='',
                                    voterId=user_id),
                               room=room,
                               skip_sid=sid)
            except AttributeError:
                pass
    log.debug(f'Client disconnected: {sid} (id: {user_id})')


@sio.on('create_room')
async def create_room(sid, data):
    """
    Master call
    Repeated calls from same session must not create new rooms.
    :param sid:
    :param data: {
        roomName:str
    }
    :return: {
        roomId:str,
        userId:str,
        roomName:str,
        currentTopic:str|null,
        voters:list[{id:str, username:str, avatar:url, vote:number|null}]
    }
    """
    room_name = data['roomName']
    session = await sio.get_session(sid)
    room_id, user_id = rand_id(), session['user_id']

    PokerStore.add_poker(room_id, ScrumPoker(user_id, room_name))
    log.info(f'Room {room_name} ({room_id}) created by user {user_id}')

    sio.enter_room(sid, room_id)
    return dict(
        roomId=room_id,
        userId=user_id,
        roomName=room_name
    )


@sio.on('join_room')
async def join_room(sid, data):
    """
    Voter call, join existing room
    Repeated calls must not duplicate user
    :param sid:
    :param data: {
        roomId:str,
        voterName:str
    }
    :return: {
        roomId:str,
        voterName:str,
        voterId:str,
        currentTopic:str|null
    }
    """
    session = await sio.get_session(sid)
    user_id, avatar = session['user_id'], session['avatar']

    room_id, name = data['roomId'], data['voterName']

    # one room per session
    for room in sio.rooms(sid):
        if room != sid:
            sio.leave_room(sid, room)
            try:
                p = PokerStore.get_poker(room)
                p.remove_member(user_id)
            except AttributeError:
                pass

    poker = PokerStore.get_poker(room_id)
    if not poker:
        return {'error': 'no-room'}
    poker.add_member(user_id, {'name': name, 'avatar': avatar})

    log.debug(f'Members in room {room_id} -> {poker.members}')

    sio.enter_room(sid, room_id)
    await sio.emit('voter_joined',
                   dict(voterName=name,
                        voterId=user_id,
                        voterAvatar=avatar),
                   room=room_id,
                   skip_sid=sid)

    log.info(f'Voter {name} ({user_id}) joined room: {room_id}')

    return dict(
        voterId=user_id,
        voterName=name,
        roomId=room_id,
        currentTopic=poker.current_topic
    )


@sio.on('master_rejoin')
async def master_rejoin(sid, data):
    """
    Called when master re-visits the Master page.
    Override userId generator
    :param sid:
    :param data: {
        userId:str
        roomId:str
    }
    :return: {
        roomId:str,
        userId:str,
        roomName:str,
        topics:list[{title:str, value:str}], --- memo za v2
        currentTopic:str|null,
        voters:list[{id:str, username:str, avatar:url, vote:number|null
    }]
    """
    user_id, room_id = data['userId'], data['roomId']
    async with sio.session(sid) as session:
        session['user_id'] = user_id
    sio.enter_room(sid, room_id)

    poker = PokerStore.get_poker(room_id)

    if not poker:
        return {'error': 'no-room'}

    log.info(f'Master rejoined room: {room_id}')

    return dict(
        roomId=room_id,
        userId=user_id,
        roomName=poker.name,
        currentTopic=poker.current_topic,
        voters=[{'voterId': iid,
                 'voterName': dt['name'],
                 'vote': poker.get_vote(iid),
                 'voterAvatar': dt['avatar']} for iid, dt in poker.members.items()]
    )


@sio.on('next_topic')
async def change_topic(sid, data):
    """

    :param sid:
    :param data: {
        topic:str
    }
    :return: {
        success:bool
    }
    """
    topic = data['topic']
    room_id = list(filter(lambda s: s != sid, sio.rooms(sid)))[0]
    poker = PokerStore.get_poker(room_id)

    if not poker:
        return {'error': 'no-room'}

    poker.new_topic(topic)
    log.info(f'Topic changed {topic}')

    await sio.emit('topic_changed',
                   {'topic': topic},
                   room=room_id)
    return True


@sio.on('vote')
async def vote(sid, data):
    """

    :param sid:
    :param data: {vote:str}
    :return:
    """
    session = await sio.get_session(sid)
    user_id = session['user_id']
    value = data['vote']

    room_id = list(filter(lambda s: s != sid, sio.rooms(sid)))[0]
    poker = PokerStore.get_poker(room_id)
    if not poker:
        return {'error': 'no-room'}
    poker.vote(user_id, value)

    # fix: emits to whole room, only master needs the event
    log.info(f'Vote submitted f{value} by {user_id}')
    await sio.emit('voter_action',
                   dict(type='vote',
                        value=value,
                        voterId=user_id),
                   room=room_id,
                   skip_sid=sid)

    if poker.is_voting_finished():
        log.info(f'Vote finished for topic {poker.current_topic}')
        await sio.emit('vote_finished',
                       dict(voters=[{'voterId': iid,
                                     'voterName': dt['name'],
                                     'voterAvatar': dt['avatar'],
                                     'vote': poker.get_vote(iid)} for iid, dt in poker.members.items()]),
                       room=room_id)

    return {'success': True}







