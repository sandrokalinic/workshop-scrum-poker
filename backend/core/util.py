import random
from core.scrumpoker import ScrumPoker
import string


def rand_id():
    return str(random.randint(100000, 999999))


def rand_img():
    rnd = ''.join(random.choice(string.ascii_letters) for _ in range(10))
    return f'https://api.adorable.io/avatars/50/{rnd}.png'


class PokerStore(object):
    _store = {}

    @classmethod
    def add_poker(cls, iid, poker):
        cls._store[iid] = poker

    @classmethod
    def get_poker(cls, iid) -> ScrumPoker:
        try:
            return cls._store[iid]
        except KeyError:
            return None
