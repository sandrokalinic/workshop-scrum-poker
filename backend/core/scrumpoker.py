
class ScrumPoker(object):

    def __init__(self, owner, name):
        self.owner = owner
        self.name = name
        self.topics = []
        self.members = {}
        # id: vote
        self.current_votes = {}

    @property
    def current_topic(self):
        return self.topics[-1] if len(self.topics) > 0 else None

    def get_vote(self, member_id):
        try:
            return self.current_votes[member_id]
        except KeyError:
            return None

    def vote(self, member_id, vote):
        if member_id in self.members:
            self.current_votes[member_id] = vote

    def is_voting_finished(self):
        return len(self.current_votes) == len(self.members)

    def new_topic(self, name):
        self.topics.append(name)
        self.current_votes = {}

    def add_member(self, iid, meta):
        self.members[iid] = meta

    def remove_member(self, iid):
        try:
            del self.members[iid]
            del self.current_votes[iid]
        except KeyError:
            pass
